﻿namespace SurveyAnalyzer

module Series =

    open QuestionnaireCommon
    open Session

    open System.Collections.Generic
    
    type Series =
        { 
            Values              : float[]
            Mean                : float
            Variance            : float
        }
    
    let makeSeries(sessionData : SessionData[]) = 
                              
        let seriesDic = new Dictionary<string, Series[]>()

        let makeSeries(dependentVariables : float[][]) =
            
            let numPoints = if dependentVariables.Length > 0 then dependentVariables.[0].Length-1 else 0
            let series = 
                [|
                    for y in dependentVariables do
                        let mean = (y |> Array.sum) / (float y.Length)
                        let variance = (y |> Array.map(fun x -> 
                                                        let divergence = x - mean
                                                        divergence * divergence
                                                     ) 
                                         |> Array.sum)  / (float y.Length)
                        let newSeries =    {   
                                                Values = y
                                                Mean = mean
                                                Variance = variance
                                            }
                        yield newSeries
                |]
            series

        let addSeriesForGrabbing(key : string, dependentVariables : float[][]) =
            seriesDic.Add(key + "_0123", makeSeries([| dependentVariables.[0]; dependentVariables.[1]; dependentVariables.[2]; dependentVariables.[3] |]) )
            seriesDic.Add(key + "_01",   makeSeries([| dependentVariables.[0]; dependentVariables.[1] |]) )
            seriesDic.Add(key + "_02",   makeSeries([| dependentVariables.[0]; dependentVariables.[2] |]) )
            seriesDic.Add(key + "_03",   makeSeries([| dependentVariables.[0]; dependentVariables.[3] |]) )
            seriesDic.Add(key + "_12",   makeSeries([| dependentVariables.[1]; dependentVariables.[2] |]) )
            seriesDic.Add(key + "_13",   makeSeries([| dependentVariables.[1]; dependentVariables.[3] |]) )
            seriesDic.Add(key + "_23",   makeSeries([| dependentVariables.[2]; dependentVariables.[3] |]) )
            
            // compare with https://www.youtube.com/watch?v=-yQb_ZJnFXw
    //        seriesDic.Add("Test", makeSeries([| [| 2.0; 3.0; 7.0; 2.0; 6.0 |]; [| 10.0; 8.0; 7.0; 5.0; 10.0 |]; [| 10.0; 13.0; 14.0; 13.0; 15.0 |]; |]) )

        if sessionData.Length > 0 then
            let numPresenceQuestions = sessionData.[0].PresenceGrabbingNoFB.Answers.Length
            let numTaskLoadQuestions = sessionData.[0].UsabilityGrabbingBothFB.Answers.Length
        
            // basketball
            seriesDic.Add("BasketballScore", makeSeries([| sessionData |> Array.map (fun s -> s.Scores.BasketballScore) |]))

            let basketballTaskLoad = [| for i in [|0..numTaskLoadQuestions-1|] do yield sessionData |> Array.map (fun s -> s.UsabilityBasketball.Answers.[i]) |]
            for i in [|0..numTaskLoadQuestions-1|] do
                seriesDic.Add("BasketballUsabilitySeries_" + i.ToString() + "_" + sessionData.[0].UsabilityBasketball.Questions.[i], 
                                makeSeries([| basketballTaskLoad.[i] |]) )
            seriesDic.Add("BasketballUsabilitySeries",   makeSeries([| sessionData |> Array.map (fun s -> s.UsabilityBasketball.UsabilityScore) |]) )
            seriesDic.Add("BasketballTaskLoadSeries",   makeSeries([| sessionData |> Array.map (fun s -> s.UsabilityBasketball.TaskLoadScore) |]) )

            // grabbing
            let presenceGrabbingNoFBAllAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingNoFB.Avg.allAvg)
            let presenceGrabbingNoFBReaAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingNoFB.Avg.realismAvg)
            let presenceGrabbingNoFBActAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingNoFB.Avg.actAvg)
            let presenceGrabbingNoFBIntAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingNoFB.Avg.interfaceAvg)
            let presenceGrabbingNoFBExaAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingNoFB.Avg.examineAvg)
            let presenceGrabbingNoFBPerAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingNoFB.Avg.performanceAvg)
            let presenceGrabbingNoFBSouAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingNoFB.Avg.soundAvg)
            let presenceGrabbingNoFBHapAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingNoFB.Avg.hapticAvg)
            
            let presenceGrabbingHapticAllAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingHaptic.Avg.allAvg)
            let presenceGrabbingHapticReaAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingHaptic.Avg.realismAvg)
            let presenceGrabbingHapticActAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingHaptic.Avg.actAvg)
            let presenceGrabbingHapticIntAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingHaptic.Avg.interfaceAvg)
            let presenceGrabbingHapticExaAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingHaptic.Avg.examineAvg)
            let presenceGrabbingHapticPerAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingHaptic.Avg.performanceAvg)
            let presenceGrabbingHapticSouAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingHaptic.Avg.soundAvg)
            let presenceGrabbingHapticHapAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingHaptic.Avg.hapticAvg)
            
            let presenceGrabbingOpticalAllAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingOptical.Avg.allAvg)
            let presenceGrabbingOpticalReaAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingOptical.Avg.realismAvg)
            let presenceGrabbingOpticalActAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingOptical.Avg.actAvg)
            let presenceGrabbingOpticalIntAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingOptical.Avg.interfaceAvg)
            let presenceGrabbingOpticalExaAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingOptical.Avg.examineAvg)
            let presenceGrabbingOpticalPerAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingOptical.Avg.performanceAvg)
            let presenceGrabbingOpticalSouAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingOptical.Avg.soundAvg)
            let presenceGrabbingOpticalHapAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingOptical.Avg.hapticAvg)
            
            let presenceGrabbingBothAllAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingBothFB.Avg.allAvg)
            let presenceGrabbingBothReaAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingBothFB.Avg.realismAvg)
            let presenceGrabbingBothActAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingBothFB.Avg.actAvg)
            let presenceGrabbingBothIntAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingBothFB.Avg.interfaceAvg)
            let presenceGrabbingBothExaAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingBothFB.Avg.examineAvg)
            let presenceGrabbingBothPerAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingBothFB.Avg.performanceAvg)
            let presenceGrabbingBothSouAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingBothFB.Avg.soundAvg)
            let presenceGrabbingBothHapAvg = sessionData |> Array.map (fun s -> s.PresenceGrabbingBothFB.Avg.hapticAvg)

            // Grabbing All Presence
            addSeriesForGrabbing("GrabbingPresenceAllSeries",          [| presenceGrabbingNoFBAllAvg; presenceGrabbingHapticAllAvg; presenceGrabbingOpticalAllAvg; presenceGrabbingBothAllAvg |])
            addSeriesForGrabbing("GrabbingPresenceRealismSeries",      [| presenceGrabbingNoFBReaAvg; presenceGrabbingHapticReaAvg; presenceGrabbingOpticalReaAvg; presenceGrabbingBothReaAvg |])
            addSeriesForGrabbing("GrabbingPresenceActionSeries",       [| presenceGrabbingNoFBActAvg; presenceGrabbingHapticActAvg; presenceGrabbingOpticalActAvg; presenceGrabbingBothActAvg |])
            addSeriesForGrabbing("GrabbingPresenceInterfaceSeries",    [| presenceGrabbingNoFBIntAvg; presenceGrabbingHapticIntAvg; presenceGrabbingOpticalIntAvg; presenceGrabbingBothIntAvg |])
            addSeriesForGrabbing("GrabbingPresenceExamineSeries",      [| presenceGrabbingNoFBExaAvg; presenceGrabbingHapticExaAvg; presenceGrabbingOpticalExaAvg; presenceGrabbingBothExaAvg |])
            addSeriesForGrabbing("GrabbingPresencePerformanceSeries",  [| presenceGrabbingNoFBPerAvg; presenceGrabbingHapticPerAvg; presenceGrabbingOpticalPerAvg; presenceGrabbingBothPerAvg |])
            addSeriesForGrabbing("GrabbingPresenceSoundSeries",        [| presenceGrabbingNoFBSouAvg; presenceGrabbingHapticSouAvg; presenceGrabbingOpticalSouAvg; presenceGrabbingBothSouAvg |])
            addSeriesForGrabbing("GrabbingPresenceHapticSeries",       [| presenceGrabbingNoFBHapAvg; presenceGrabbingHapticHapAvg; presenceGrabbingOpticalHapAvg; presenceGrabbingBothHapAvg |])

            // Grabbing Presence Single Questions
            let grabbingAnswersNoFB = [| for i in [|0..numPresenceQuestions-1|] do yield sessionData |> Array.map (fun s -> s.PresenceGrabbingNoFB.Answers.[i]) |]
            let grabbingAnswersHapt = [| for i in [|0..numPresenceQuestions-1|] do yield sessionData |> Array.map (fun s -> s.PresenceGrabbingHaptic.Answers.[i]) |]
            let grabbingAnswersOpti = [| for i in [|0..numPresenceQuestions-1|] do yield sessionData |> Array.map (fun s -> s.PresenceGrabbingOptical.Answers.[i]) |]
            let grabbingAnswersBoth = [| for i in [|0..numPresenceQuestions-1|] do yield sessionData |> Array.map (fun s -> s.PresenceGrabbingBothFB.Answers.[i]) |]
                
            for i in [|0..numPresenceQuestions-1|] do
                addSeriesForGrabbing("GrabbingPresenceSeries_" + i.ToString() + "_" + sessionData.[0].PresenceGrabbingBothFB.Questions.[i],   
                                [| grabbingAnswersNoFB.[i]; grabbingAnswersHapt.[i]; grabbingAnswersOpti.[i]; grabbingAnswersBoth.[i] |])

            // Grabbing Scores
            let noFBScoreSum = sessionData |> Array.map (fun s -> s.Scores.NoFeedback.Score.Round0 + s.Scores.NoFeedback.Score.Round1 + s.Scores.NoFeedback.Score.Round2 + s.Scores.NoFeedback.Score.Round3)
            let hapticFBScoreSum = sessionData |> Array.map (fun s -> s.Scores.HapticFeedback.Score.Round0 + s.Scores.HapticFeedback.Score.Round1 + s.Scores.HapticFeedback.Score.Round2 + s.Scores.HapticFeedback.Score.Round3)
            let opticalFBScoreSum = sessionData |> Array.map (fun s -> s.Scores.OpticalFeedback.Score.Round0 + s.Scores.OpticalFeedback.Score.Round1 + s.Scores.OpticalFeedback.Score.Round2 + s.Scores.OpticalFeedback.Score.Round3)
            let bothFBScoreSum = sessionData |> Array.map (fun s -> s.Scores.BothFeedback.Score.Round0 + s.Scores.BothFeedback.Score.Round1 + s.Scores.BothFeedback.Score.Round2 + s.Scores.BothFeedback.Score.Round3)
            addSeriesForGrabbing("GrabbingScoresAllRoundsSeries",          [| noFBScoreSum; hapticFBScoreSum; opticalFBScoreSum; bothFBScoreSum |])
            addSeriesForGrabbing("GrabbingScoresSeriesRound0",             [| sessionData |> Array.map (fun s -> s.Scores.NoFeedback.Score.Round0);
                                                                                  sessionData |> Array.map (fun s -> s.Scores.HapticFeedback.Score.Round0);
                                                                                  sessionData |> Array.map (fun s -> s.Scores.OpticalFeedback.Score.Round0);
                                                                                  sessionData |> Array.map (fun s -> s.Scores.BothFeedback.Score.Round0) |])
            addSeriesForGrabbing("GrabbingScoresSeriesRound1",             [| sessionData |> Array.map (fun s -> s.Scores.NoFeedback.Score.Round1);
                                                                                  sessionData |> Array.map (fun s -> s.Scores.HapticFeedback.Score.Round1);
                                                                                  sessionData |> Array.map (fun s -> s.Scores.OpticalFeedback.Score.Round1);
                                                                                  sessionData |> Array.map (fun s -> s.Scores.BothFeedback.Score.Round1) |])
            addSeriesForGrabbing("GrabbingScoresSeriesRound2",             [| sessionData |> Array.map (fun s -> s.Scores.NoFeedback.Score.Round2);
                                                                                  sessionData |> Array.map (fun s -> s.Scores.HapticFeedback.Score.Round2);
                                                                                  sessionData |> Array.map (fun s -> s.Scores.OpticalFeedback.Score.Round2);
                                                                                  sessionData |> Array.map (fun s -> s.Scores.BothFeedback.Score.Round2) |])
            addSeriesForGrabbing("GrabbingScoresSeriesRound3",             [| sessionData |> Array.map (fun s -> s.Scores.NoFeedback.Score.Round3);
                                                                                  sessionData |> Array.map (fun s -> s.Scores.HapticFeedback.Score.Round3);
                                                                                  sessionData |> Array.map (fun s -> s.Scores.OpticalFeedback.Score.Round3);
                                                                                  sessionData |> Array.map (fun s -> s.Scores.BothFeedback.Score.Round3) |])

            // Grabbing Scores Over No Feedback
            let hapticScoreSumOverNoFB = [| for i in [| 0..noFBScoreSum.Length-1 |] do yield hapticFBScoreSum.[i] / noFBScoreSum.[i] |]
            let opticaScoreSumOverNoFB = [| for i in [| 0..noFBScoreSum.Length-1 |] do yield opticalFBScoreSum.[i] / noFBScoreSum.[i] |]
            let bothFBScoreSumOverNoFB = [| for i in [| 0..noFBScoreSum.Length-1 |] do yield bothFBScoreSum.[i] / noFBScoreSum.[i] |]
            seriesDic.Add("GrabbingScoresSumOverNoFB", makeSeries([| hapticScoreSumOverNoFB; opticaScoreSumOverNoFB; bothFBScoreSumOverNoFB |]) )
            
            // Scores Over First Round
            let noFBScoreOverFirstRoundSum = sessionData |> Array.map (fun s -> (s.Scores.NoFeedback.Score.Round1 + s.Scores.NoFeedback.Score.Round2 + s.Scores.NoFeedback.Score.Round3) / s.Scores.NoFeedback.Score.Round0)
            let hapticFBScoreOverFirstRoundSum = sessionData |> Array.map (fun s -> (s.Scores.HapticFeedback.Score.Round1 + s.Scores.HapticFeedback.Score.Round2 + s.Scores.HapticFeedback.Score.Round3) / s.Scores.HapticFeedback.Score.Round0)
            let opticalFBScoreOverFirstRoundSum = sessionData |> Array.map (fun s -> (s.Scores.OpticalFeedback.Score.Round1 + s.Scores.OpticalFeedback.Score.Round2 + s.Scores.OpticalFeedback.Score.Round3) / s.Scores.OpticalFeedback.Score.Round0)
            let bothFBScoreOverFirstRoundSum = sessionData |> Array.map (fun s -> (s.Scores.BothFeedback.Score.Round1 + s.Scores.BothFeedback.Score.Round2 + s.Scores.BothFeedback.Score.Round3) / s.Scores.BothFeedback.Score.Round0)
            addSeriesForGrabbing("GrabbingScoresOverFirstRoundAllRounds", [| noFBScoreOverFirstRoundSum; hapticFBScoreOverFirstRoundSum; opticalFBScoreOverFirstRoundSum; bothFBScoreOverFirstRoundSum |])
        
            // Grabs
            let noFBGrabsSum = sessionData |> Array.map (fun s -> s.Scores.NoFeedback.Grabs.Round0 + s.Scores.NoFeedback.Grabs.Round1 + s.Scores.NoFeedback.Grabs.Round2 + s.Scores.NoFeedback.Grabs.Round3)
            let hapticFBGrabsSum = sessionData |> Array.map (fun s -> s.Scores.HapticFeedback.Grabs.Round0 + s.Scores.HapticFeedback.Grabs.Round1 + s.Scores.HapticFeedback.Grabs.Round2 + s.Scores.HapticFeedback.Grabs.Round3)
            let opticalFBGrabsSum = sessionData |> Array.map (fun s -> s.Scores.OpticalFeedback.Grabs.Round0 + s.Scores.OpticalFeedback.Grabs.Round1 + s.Scores.OpticalFeedback.Grabs.Round2 + s.Scores.OpticalFeedback.Grabs.Round3)
            let bothFBGrabsSum = sessionData |> Array.map (fun s -> s.Scores.BothFeedback.Grabs.Round0 + s.Scores.BothFeedback.Grabs.Round1 + s.Scores.BothFeedback.Grabs.Round2 + s.Scores.BothFeedback.Grabs.Round3)
            addSeriesForGrabbing("GrabbingGrabsSeriesAllRounds", [| noFBGrabsSum; hapticFBGrabsSum; opticalFBGrabsSum; bothFBGrabsSum |] )
        
            // Grabs Over First Round
            let noFBGrabsOverFirstRoundSum = sessionData |> Array.map (fun s -> (s.Scores.NoFeedback.Grabs.Round1 + s.Scores.NoFeedback.Grabs.Round2 + s.Scores.NoFeedback.Grabs.Round3) / s.Scores.NoFeedback.Grabs.Round0)
            let hapticFBGrabsOverFirstRoundSum = sessionData |> Array.map (fun s -> (s.Scores.HapticFeedback.Grabs.Round1 + s.Scores.HapticFeedback.Grabs.Round2 + s.Scores.HapticFeedback.Grabs.Round3) / s.Scores.HapticFeedback.Grabs.Round0)
            let opticalFBGrabsOverFirstRoundSum = sessionData |> Array.map (fun s -> (s.Scores.OpticalFeedback.Grabs.Round1 + s.Scores.OpticalFeedback.Grabs.Round2 + s.Scores.OpticalFeedback.Grabs.Round3) / s.Scores.OpticalFeedback.Grabs.Round0)
            let bothFBGrabsOverFirstRoundSum = sessionData |> Array.map (fun s -> (s.Scores.BothFeedback.Grabs.Round1 + s.Scores.BothFeedback.Grabs.Round2 + s.Scores.BothFeedback.Grabs.Round3) / s.Scores.BothFeedback.Grabs.Round0)
            addSeriesForGrabbing("GrabbingGrabssOverFirstRoundAllRounds", [| noFBGrabsOverFirstRoundSum; hapticFBGrabsOverFirstRoundSum; opticalFBGrabsOverFirstRoundSum; bothFBGrabsOverFirstRoundSum |])
        
            // Attempts
            let noFBAttemptsSum = sessionData |> Array.map (fun s -> s.Scores.NoFeedback.Attempts.Round0 + s.Scores.NoFeedback.Attempts.Round1 + s.Scores.NoFeedback.Attempts.Round2 + s.Scores.NoFeedback.Attempts.Round3)
            let hapticFBAttemptsSum = sessionData |> Array.map (fun s -> s.Scores.HapticFeedback.Attempts.Round0 + s.Scores.HapticFeedback.Attempts.Round1 + s.Scores.HapticFeedback.Attempts.Round2 + s.Scores.HapticFeedback.Attempts.Round3)
            let opticalFBAttemptsSum = sessionData |> Array.map (fun s -> s.Scores.OpticalFeedback.Attempts.Round0 + s.Scores.OpticalFeedback.Attempts.Round1 + s.Scores.OpticalFeedback.Attempts.Round2 + s.Scores.OpticalFeedback.Attempts.Round3)
            let bothFBAttemptsSum = sessionData |> Array.map (fun s -> s.Scores.BothFeedback.Attempts.Round0 + s.Scores.BothFeedback.Attempts.Round1 + s.Scores.BothFeedback.Attempts.Round2 + s.Scores.BothFeedback.Attempts.Round3)
            addSeriesForGrabbing("GrabbingAttemptsSeriesAllRounds", [| noFBAttemptsSum; hapticFBAttemptsSum; opticalFBAttemptsSum; bothFBAttemptsSum |] )
            
            // Attempts Over First Round
            let noFBAttemptsOverFirstRoundSum = sessionData |> Array.map (fun s -> (s.Scores.NoFeedback.Attempts.Round1 + s.Scores.NoFeedback.Attempts.Round2 + s.Scores.NoFeedback.Attempts.Round3) / s.Scores.NoFeedback.Attempts.Round0)
            let hapticFBAttemptsOverFirstRoundSum = sessionData |> Array.map (fun s -> (s.Scores.HapticFeedback.Attempts.Round1 + s.Scores.HapticFeedback.Attempts.Round2 + s.Scores.HapticFeedback.Attempts.Round3) / s.Scores.HapticFeedback.Attempts.Round0)
            let opticalFBAttemptsOverFirstRoundSum = sessionData |> Array.map (fun s -> (s.Scores.OpticalFeedback.Attempts.Round1 + s.Scores.OpticalFeedback.Attempts.Round2 + s.Scores.OpticalFeedback.Attempts.Round3) / s.Scores.OpticalFeedback.Attempts.Round0)
            let bothFBAttemptsOverFirstRoundSum = sessionData |> Array.map (fun s -> (s.Scores.BothFeedback.Attempts.Round1 + s.Scores.BothFeedback.Attempts.Round2 + s.Scores.BothFeedback.Attempts.Round3) / s.Scores.BothFeedback.Attempts.Round0)
            addSeriesForGrabbing("GrabbingAttemptssOverFirstRoundAllRounds", [| noFBAttemptsOverFirstRoundSum; hapticFBAttemptsOverFirstRoundSum; opticalFBAttemptsOverFirstRoundSum; bothFBAttemptsOverFirstRoundSum |])
        
            // Grabs Over Attempts
            let noFBGrabsOverAttemptsSum = ([|0..noFBGrabsSum.Length-1|]) |> Array.map (fun i -> noFBGrabsSum.[i] / noFBAttemptsSum.[i])
            let hapticFBGrabsOverAttemptsSum = ([|0..hapticFBGrabsSum.Length-1|]) |> Array.map (fun i -> hapticFBGrabsSum.[i] / hapticFBAttemptsSum.[i])
            let opticalFBGrabsOverAttemptsSum = ([|0..opticalFBGrabsSum.Length-1|]) |> Array.map (fun i -> opticalFBGrabsSum.[i] / opticalFBAttemptsSum.[i])
            let bothFBGrabsOverAttemptsSum = ([|0..bothFBGrabsSum.Length-1|]) |> Array.map (fun i -> bothFBGrabsSum.[i] / bothFBAttemptsSum.[i])
            addSeriesForGrabbing("GrabbingGrabsOverAttemptsSeriesAllRounds", [| noFBGrabsOverAttemptsSum; hapticFBGrabsOverAttemptsSum; opticalFBGrabsOverAttemptsSum; bothFBGrabsOverAttemptsSum |] )
            
            // Grabs Over Attempts Over First Round
            let noFBGrabsOverAttemptsOverFirstRoundSum = ([|0..noFBGrabsOverFirstRoundSum.Length-1|]) |> Array.map (fun i -> noFBGrabsOverFirstRoundSum.[i] / noFBAttemptsOverFirstRoundSum.[i])
            let hapticFBGrabsOverAttemptsOverFirstRoundSum = ([|0..hapticFBGrabsOverFirstRoundSum.Length-1|]) |> Array.map (fun i -> hapticFBGrabsOverFirstRoundSum.[i] / hapticFBAttemptsOverFirstRoundSum.[i])
            let opticalFBGrabsOverAttemptsOverFirstRoundSum = ([|0..opticalFBGrabsOverFirstRoundSum.Length-1|]) |> Array.map (fun i -> opticalFBGrabsOverFirstRoundSum.[i] / opticalFBAttemptsOverFirstRoundSum.[i])
            let bothFBGrabsOverAttemptsOverFirstRoundSum = ([|0..bothFBGrabsOverFirstRoundSum.Length-1|]) |> Array.map (fun i -> bothFBGrabsOverFirstRoundSum.[i] / bothFBAttemptsOverFirstRoundSum.[i])
            addSeriesForGrabbing("GrabbingGrabsOverAttemptsOverFirstRoundSeriesAllRounds", [| noFBGrabsOverAttemptsOverFirstRoundSum; hapticFBGrabsOverAttemptsOverFirstRoundSum; opticalFBGrabsOverAttemptsOverFirstRoundSum; bothFBGrabsOverAttemptsOverFirstRoundSum |] )

            // Task Load / Usability
            let grabbingTaskLoadNoFB = [| for i in [|0..numTaskLoadQuestions-1|] do yield sessionData |> Array.map (fun s -> s.UsabilityGrabbingNoFB.Answers.[i]) |]
            let grabbingTaskLoadHapt = [| for i in [|0..numTaskLoadQuestions-1|] do yield sessionData |> Array.map (fun s -> s.UsabilityGrabbingHaptic.Answers.[i]) |]
            let grabbingTaskLoadOpti = [| for i in [|0..numTaskLoadQuestions-1|] do yield sessionData |> Array.map (fun s -> s.UsabilityGrabbingOptical.Answers.[i]) |]
            let grabbingTaskLoadBoth = [| for i in [|0..numTaskLoadQuestions-1|] do yield sessionData |> Array.map (fun s -> s.UsabilityGrabbingBothFB.Answers.[i]) |]

            for i in [|0..numTaskLoadQuestions-1|] do
                addSeriesForGrabbing("GrabbingTaskLoadSeries_" + i.ToString() + "_" + sessionData.[0].UsabilityGrabbingNoFB.Questions.[i], 
                                [| grabbingTaskLoadNoFB.[i]; grabbingTaskLoadHapt.[i]; grabbingTaskLoadOpti.[i]; grabbingTaskLoadBoth.[i] |])
                                
            addSeriesForGrabbing("GrabbingUsabilitySeries", [| sessionData |> Array.map (fun s -> s.UsabilityGrabbingNoFB.UsabilityScore); sessionData |> Array.map (fun s -> s.UsabilityGrabbingHaptic.UsabilityScore); sessionData |> Array.map (fun s -> s.UsabilityGrabbingOptical.UsabilityScore); sessionData |> Array.map (fun s -> s.UsabilityGrabbingBothFB.UsabilityScore) |])
            addSeriesForGrabbing("GrabbingTaskLoadSeries",  [| sessionData |> Array.map (fun s -> s.UsabilityGrabbingNoFB.TaskLoadScore); sessionData |> Array.map (fun s -> s.UsabilityGrabbingHaptic.TaskLoadScore); sessionData |> Array.map (fun s -> s.UsabilityGrabbingOptical.TaskLoadScore); sessionData |> Array.map (fun s -> s.UsabilityGrabbingBothFB.TaskLoadScore) |])
                       
            // Point-Clouds
            seriesDic.Add("PointCloudScoresWrongPoints", makeSeries([| sessionData |> Array.map (fun s -> (s.Scores.PCDesktop.wronglySelected + s.Scores.PCDesktop.wronglyNonSelected)); sessionData |> Array.map (fun s -> (s.Scores.PCVirtualReality.wronglySelected + s.Scores.PCVirtualReality.wronglyNonSelected)) |]) )
            seriesDic.Add("PointCloudScoresWronglySelectedPoints", makeSeries([| sessionData |> Array.map (fun s -> (s.Scores.PCDesktop.wronglySelected)); sessionData |> Array.map (fun s -> (s.Scores.PCVirtualReality.wronglySelected)) |]) )
            seriesDic.Add("PointCloudScoresWronglyNonSelectedPoints", makeSeries([| sessionData |> Array.map (fun s -> (s.Scores.PCDesktop.wronglyNonSelected)); sessionData |> Array.map (fun s -> (s.Scores.PCVirtualReality.wronglyNonSelected)) |]) )
            seriesDic.Add("PointCloudScoresOperations", makeSeries([| sessionData |> Array.map (fun s -> (s.Scores.PCDesktop.selectionOperations + s.Scores.PCDesktop.deselectOperations)); sessionData |> Array.map (fun s -> (s.Scores.PCVirtualReality.selectionOperations + s.Scores.PCVirtualReality.deselectOperations)) |]) )
            seriesDic.Add("PointCloudScoresDuration", makeSeries([| sessionData |> Array.map (fun s -> (s.Scores.PCDesktop.duration)); sessionData |> Array.map (fun s -> (s.Scores.PCVirtualReality.duration)) |]) )
 
            for i in [|0..numTaskLoadQuestions-1|] do
                seriesDic.Add("PointCloudTaskLoadSeries_" + i.ToString() + "_" + sessionData.[0].UsabilityPointCloudPC.Questions.[i],   makeSeries([| sessionData |> Array.map (fun s -> s.UsabilityPointCloudPC.Answers.[i]); sessionData |> Array.map (fun s -> s.UsabilityPointCloudVR.Answers.[i]) |]) )

            seriesDic.Add("PointCloudUsabilitySeries",   makeSeries([| sessionData |> Array.map (fun s -> s.UsabilityPointCloudPC.UsabilityScore); sessionData |> Array.map (fun s -> s.UsabilityPointCloudVR.UsabilityScore) |]) )
            seriesDic.Add("PointCloudTaskLoadSeries",   makeSeries([| sessionData |> Array.map (fun s -> s.UsabilityPointCloudPC.TaskLoadScore); sessionData |> Array.map (fun s -> s.UsabilityPointCloudVR.TaskLoadScore) |]) )

            // Simulator Sickness
            let numSicknessQuestions = sessionData.[0].SicknessBasketball.Answers.Length
            for i in [|0..numSicknessQuestions-1|] do
                seriesDic.Add("AllAppsSicknessSeries_" + i.ToString() + "_" + sessionData.[0].SicknessBasketball.Questions.[i],   makeSeries([| sessionData |> Array.map (fun s -> s.SicknessBasketball.Answers.[i]); sessionData |> Array.map (fun s -> s.SicknessGrabbing.Answers.[i]); sessionData |> Array.map (fun s -> s.SicknessPointCloud.Answers.[i]) |]) )
                
            // Basketball vs. point-clouds presence
            let presencePointCloudAllAvg = sessionData |> Array.map (fun s -> s.PresencePointCloudVR.Avg.allAvg)
            let presencePointCloudReaAvg = sessionData |> Array.map (fun s -> s.PresencePointCloudVR.Avg.realismAvg)
            let presencePointCloudActAvg = sessionData |> Array.map (fun s -> s.PresencePointCloudVR.Avg.actAvg)
            let presencePointCloudIntAvg = sessionData |> Array.map (fun s -> s.PresencePointCloudVR.Avg.interfaceAvg)
            let presencePointCloudExaAvg = sessionData |> Array.map (fun s -> s.PresencePointCloudVR.Avg.examineAvg)
            let presencePointCloudPerAvg = sessionData |> Array.map (fun s -> s.PresencePointCloudVR.Avg.performanceAvg)
            let presencePointCloudSouAvg = sessionData |> Array.map (fun s -> s.PresencePointCloudVR.Avg.soundAvg)
            let presencePointCloudHapAvg = sessionData |> Array.map (fun s -> s.PresencePointCloudVR.Avg.hapticAvg)
            
            let presenceBasketballAllAvg = sessionData |> Array.map (fun s -> s.PresenceBasketball.Avg.allAvg)
            let presenceBasketballReaAvg = sessionData |> Array.map (fun s -> s.PresenceBasketball.Avg.realismAvg)
            let presenceBasketballActAvg = sessionData |> Array.map (fun s -> s.PresenceBasketball.Avg.actAvg)
            let presenceBasketballIntAvg = sessionData |> Array.map (fun s -> s.PresenceBasketball.Avg.interfaceAvg)
            let presenceBasketballExaAvg = sessionData |> Array.map (fun s -> s.PresenceBasketball.Avg.examineAvg)
            let presenceBasketballPerAvg = sessionData |> Array.map (fun s -> s.PresenceBasketball.Avg.performanceAvg)
            let presenceBasketballSouAvg = sessionData |> Array.map (fun s -> s.PresenceBasketball.Avg.soundAvg)
            let presenceBasketballHapAvg = sessionData |> Array.map (fun s -> s.PresenceBasketball.Avg.hapticAvg)

            let basketballAnswers = [| for i in [|0..numPresenceQuestions-1|] do yield sessionData |> Array.map (fun s -> s.PresenceBasketball.Answers.[i]) |]
            let pointCloudAnswers = [| for i in [|0..numPresenceQuestions-1|] do yield sessionData |> Array.map (fun s -> s.PresencePointCloudVR.Answers.[i]) |]
            
            seriesDic.Add("BasketballPointCloudPresenceAllSeries",          makeSeries([| presenceBasketballAllAvg; presencePointCloudAllAvg |]))
            seriesDic.Add("BasketballPointCloudPresenceRealismSeries",      makeSeries([| presenceBasketballReaAvg; presencePointCloudReaAvg |]))
            seriesDic.Add("BasketballPointCloudPresenceActionSeries",       makeSeries([| presenceBasketballActAvg; presencePointCloudActAvg |]))
            seriesDic.Add("BasketballPointCloudPresenceInterfaceSeries",    makeSeries([| presenceBasketballIntAvg; presencePointCloudIntAvg |]))
            seriesDic.Add("BasketballPointCloudPresenceExamineSeries",      makeSeries([| presenceBasketballExaAvg; presencePointCloudExaAvg |]))
            seriesDic.Add("BasketballPointCloudPresencePerformanceSeries",  makeSeries([| presenceBasketballPerAvg; presencePointCloudPerAvg |]))
            seriesDic.Add("BasketballPointCloudPresenceSoundSeries",        makeSeries([| presenceBasketballSouAvg; presencePointCloudSouAvg |]))
            seriesDic.Add("BasketballPointCloudPresenceHapticSeries",       makeSeries([| presenceBasketballHapAvg; presencePointCloudHapAvg |]))
            
            for i in [|0..numPresenceQuestions-1|] do
                seriesDic.Add("BasketballPointCloudPresenceSeries_" + i.ToString() + "_" + sessionData.[0].PresencePointCloudVR.Questions.[i], makeSeries([| basketballAnswers.[i]; pointCloudAnswers.[i]|]))
            

        seriesDic
