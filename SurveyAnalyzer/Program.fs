﻿namespace SurveyAnalyzer

module SurveyAnalyzer =

    open QuestionnaireCommon
    open PresenceQuestionnaire
    open ScoresQuestionnaire
    open SicknessQuestionnaire

    open System
    open System.Collections.Generic
    open System.Windows.Forms.DataVisualization.Charting

    let printAnovaResults (anovaResults : Statistics.Anova.Result[]) =

        for r in anovaResults do
//            if abs(r.Probability - 0.5) > 0.4 then
//            if r.Probability > 0.85 then
                Logging.log ""
                Logging.log (sprintf "\nAnova Result of %A for p=%A" (r.Name) (1.0-r.Probability))
                if r.Result.FRatio >= r.Result.FCriticalValue && r.Probability >= 0.95  then
                    Logging.log "The effect is SIGNIFICANT!"
                    Logging.log ("[F(" + r.Result.DegreeOfFreedomBetweenGroups.ToString("0") + ", " + r.Result.DegreeOfFreedomWithinGroups.ToString("0") + ") = " + 
                                 r.Result.FRatio.ToString("0.00") + ", p = " + (1.0-r.Probability).ToString() + "]")
                else
                    Logging.log "The effect is NOT SIGNIFICANT!"
                
    //            Logging.log (sprintf "SumOfSquaresBetweenGroups = %A"  (r.Result.SumOfSquaresBetweenGroups.ToString("0.00")))
    //            Logging.log (sprintf "SumOfSquaresWithinGroups = %A"  (r.Result.SumOfSquaresWithinGroups.ToString("0.00")))
    //            Logging.log (sprintf "SumOfSquaresTotal = %A"  (r.Result.SumOfSquaresTotal.ToString("0.00")))
//                Logging.log (sprintf "DegreeOfFreedomBetweenGroups = %A"  (r.Result.DegreeOfFreedomBetweenGroups.ToString("0.00")))
//                Logging.log (sprintf "DegreeOfFreedomWithinGroups = %A"  (r.Result.DegreeOfFreedomWithinGroups.ToString("0.00")))
    //            Logging.log (sprintf "DegreeOfFreedomTotal = %A"  (r.Result.DegreeOfFreedomTotal.ToString("0.00")))
    //            Logging.log (sprintf "MeanSquareVarianceBetweenGroups = %A"  (r.Result.MeanSquareVarianceBetweenGroups.ToString("0.00")))
    //            Logging.log (sprintf "MeanSquareVarianceWithinGroups = %A"  (r.Result.MeanSquareVarianceWithinGroups.ToString("0.00")))
//                Logging.log (sprintf "FCriticalValue = %A"  (r.Result.FCriticalValue.ToString("0.00")))
//                Logging.log (sprintf "FRatio = %A"  (r.Result.FRatio.ToString("0.00")))
//
//                Logging.log ("Group means: " + String.Concat(r.GroupMeans |> Array.map(fun m -> m.ToString("0.000") + ", ")))
//                Logging.log ("Group standard deviations: " + String.Concat(r.GroupVariances |> Array.map(fun m -> sqrt(m).ToString("0.000") + ", ")))
                
                let msd = [| for i in [|0..r.GroupMeans.Length-1|] do yield " & " + r.GroupMeans.[i].ToString("0.00") + " & " + sqrt(r.GroupVariances.[i]).ToString("0.00")|]
                Logging.log ("Group M,SD: " + String.Concat(msd) + @" \\")
                let msd = [| for i in [|0..r.GroupMeans.Length-1|] do yield " (M = " + r.GroupMeans.[i].ToString("0.00") + ", SD = " + sqrt(r.GroupVariances.[i]).ToString("0.00") + ") "|]
                Logging.log ("Group M,SD: " + String.Concat(msd))

    Logging.log "#####   Anova Test of VR User Study   #####"

    let rawPresenceData = QuestionnaireCommon.readData(@"..\..\..\input\Presence Questionnaire.csv", "\",\"")
    let presenceData = PresenceQuestionnaire.refineData(rawPresenceData)

    let rawScoresData = QuestionnaireCommon.readData(@"..\..\..\input\VR Test Scores.csv", ",")
    let scoresData = ScoresQuestionnaire.refineData(rawScoresData)

    let rawSicknessData = QuestionnaireCommon.readData(@"..\..\..\input\Simulator Sickness Questionnaire.csv", "\",\"")
    let sicknessData = SicknessQuestionnaire.refineData(rawSicknessData)

    let rawTaskLoadData = QuestionnaireCommon.readData(@"..\..\..\input\Usability and Task Load.csv", "\",\"")
    let taskLoadData = TaskLoadQuestionnaire.refineData(rawTaskLoadData)

    let sessions = Session.makeSessions(presenceData, scoresData, taskLoadData, sicknessData)
    let series = Series.makeSeries(sessions)
    Statistics.Anova.performAnova(series) |> printAnovaResults
    
    Logging.endSession()

    printfn "Press any key to close..."
    Console.ReadKey() |> ignore
