﻿namespace SurveyAnalyzer

module Logging = 

    open System.IO

    let startupTimeString = System.DateTime.UtcNow.ToLocalTime().ToString("yyyy-MM-dd_HH-mm-ss")
    let appName = System.AppDomain.CurrentDomain.FriendlyName
    System.IO.Directory.CreateDirectory(@"..\..\..\output\") |> ignore
    if System.IO.File.Exists(@"..\..\..\output\SurveyResults.txt") then System.IO.File.Delete(@"..\..\..\output\SurveyResults.txt")
    let sessionFileName = @"..\..\..\output\SurveyResults.txt"
    let streamWriter = new StreamWriter(sessionFileName, true)
    streamWriter.AutoFlush <- true
    streamWriter.WriteLine(startupTimeString + ": Start Analyzer")

    let log(text : string) =
        streamWriter.WriteLine(text)
        System.Console.WriteLine(text)
//        printfn "%A" text
//        
//    let log(format : Printf.TextWriterFormat<string>) =
//        let text : string = sprintf format
//        streamWriter.WriteLine(text)
//        printfn format

    let endSession() =
        let endTimeString = System.DateTime.UtcNow.ToLocalTime().ToString("yyyy-MM-dd_HH-mm-ss")
        streamWriter.WriteLine(endTimeString + ": End session")
        streamWriter.Flush() // doesn't do anything?
        streamWriter.Dispose()