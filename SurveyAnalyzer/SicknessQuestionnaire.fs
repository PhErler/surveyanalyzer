﻿namespace SurveyAnalyzer

module SicknessQuestionnaire =
    open QuestionnaireCommon

    open System
    
    type SicknessQuestionnaireData = 
        { 
            Timestamp           : string
            Name                : string
            TestType            : string
            Questions           : string[]
            Answers             : float[]
            Comment             : string
        }
        
    let refineData(rawData : string[][]) =

        let questions = rawData.[0] |> Array.map (fun q -> q.Replace("\"", "")) 
                                    |> Array.map (fun q -> q.Replace("[", "")) 
                                    |> Array.map (fun q -> q.Replace("]", ""))
                                    |> Array.map (fun q -> q.Replace(" ", "_"))
                                    |> Array.map (fun q -> q.Replace("_", "_"))
                                    |> Array.map (fun q -> q.Replace(":", "_"))

        let numContextQuestions = 3
        let numComments = 1
        let numAnswers = (questions.Length - numContextQuestions - numComments)
        let questions = Array.sub questions numContextQuestions numAnswers

        let answersData = Array.sub rawData 1 (rawData.Length - 1)

        let data = 
            [ for d in answersData do
                
                let answerToNumber(answer : string) = 
                    match answer with
                        | "None" -> 0.0
                        | "Slight" -> 0.33
                        | "Moderate" -> 0.67
                        | "Severe" -> 1.0
                        | _ -> failwith "invalid sickness value"
        

                let answerEntries = Array.sub d numContextQuestions numAnswers
                let answers = answerEntries |> Array.map(fun e -> answerToNumber(e))

                let dataEntry = 
                    {
                        Timestamp           = d.[0]
                        Name                = d.[1] //.Replace(" ", "_")
                        TestType            = d.[2].Replace("\"", "")
                        Questions           = questions
                        Answers             = answers
                        Comment             = d.[19]
                    }
                yield dataEntry
            ] |> List.toArray

        data
