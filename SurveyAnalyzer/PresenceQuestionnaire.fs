﻿namespace SurveyAnalyzer

module PresenceQuestionnaire =
    open QuestionnaireCommon

    open System
    
    type PresenceAverages =
        {
            allAvg              : float
            realismAvg          : float
            actAvg              : float
            interfaceAvg        : float
            examineAvg          : float
            performanceAvg      : float
            soundAvg            : float
            hapticAvg           : float
        }
        
    type PresenceQuestionnaireData = 
        { 
            Timestamp           : string
            Name                : string
            TestType            : string
            AudioEnabled        : bool
            Questions           : string[]
            Answers             : float[]
            Comment             : string
            Avg                 : PresenceAverages
        }
        
    let refineData(rawData : string[][]) =

        let questions = rawData.[0] |> Array.map (fun q -> q.Replace("\"", ""))

        let numContextQuestions = 4
        let numComments = 1
        let numAnswers = (questions.Length - numContextQuestions - numComments)
        let questions = Array.sub questions numContextQuestions numAnswers

        let answersData = Array.sub rawData 1 (rawData.Length - 1)

        let maxVal = 6.0

        let refinePresenceData (testType : string) (rawPresenceData : float[]) =
        
            let invertEntry(entry : float) = 1.0 - entry
        
            // invert question 14, 17, 18
            rawPresenceData.[13] <- invertEntry(rawPresenceData.[13])
            rawPresenceData.[16] <- invertEntry(rawPresenceData.[16])
            rawPresenceData.[17] <- invertEntry(rawPresenceData.[17])

            rawPresenceData
            
        let calcAverages(answers : float[]) =

            // categories
            let realismIndices =        [|2;3;4;5;6;9;12|]
            let actIndices =            [|0;1;7;8|]
            let interfaceIndices =      [|13;16;17|]
            let examineIndices =        [|10;11;18|]
            let performanceIndices =    [|14;15|]
            let soundIndices =          [|19;20;21|]
            let hapticIndices =         [|22;23|]
            let allIndices =            [|0..answers.Length-1|]

            let avgAnswer(scores : float[], indices : int[]) =
                let sumScores = scores |> Array.mapi (fun i d -> if indices |> Array.contains(i) then d else 0.0) |> Array.sum
                sumScores / (float indices.Length)

            let allScoresAvg = (Array.sum answers) / (float answers.Length)
            let realismScoresAvg = avgAnswer(answers, realismIndices)
            let actScoresAvg = avgAnswer(answers, actIndices)
            let interfaceScoresAvg = avgAnswer(answers, interfaceIndices)
            let examineScoresAvg = avgAnswer(answers, examineIndices)
            let performanceScoresAvg = avgAnswer(answers, performanceIndices)
            let soundScoresAvg = avgAnswer(answers, soundIndices)
            let hapticScoresAvg = avgAnswer(answers, hapticIndices)

            {
                    allAvg         = allScoresAvg
                    realismAvg     = realismScoresAvg
                    actAvg         = actScoresAvg
                    interfaceAvg   = interfaceScoresAvg
                    examineAvg     = examineScoresAvg
                    performanceAvg = performanceScoresAvg
                    soundAvg       = soundScoresAvg
                    hapticAvg      = hapticScoresAvg
            }

        let data = 
            [ for d in answersData do
                let answerEntries = Array.sub d numContextQuestions numAnswers
                let testType = d.[2].Replace("\"", "")
                let answers = answerEntries |> Array.map(fun e -> Double.Parse(e) / maxVal) |> refinePresenceData testType
                let avg = calcAverages(answers)

                let dataEntry = 
                    {
                        Timestamp           = d.[0]
                        Name                = d.[1] //.Replace(" ", "_")
                        TestType            = testType
                        AudioEnabled        = if d.[3] = "Yes" then true else false
                        Questions           = questions
                        Answers             = answers
                        Comment             = d.[28]
                        Avg                 = avg
                    }
                yield dataEntry
            ] |> List.toArray

        data
