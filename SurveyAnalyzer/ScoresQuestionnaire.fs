﻿namespace SurveyAnalyzer

module ScoresQuestionnaire =
    open QuestionnaireCommon

    open System

    type GrabbingTestScores = 
        { 
            Round0              : float
            Round1              : float
            Round2              : float
            Round3              : float
//            Final               : float
        }
        
    type GrabbingScoreTypes = 
        { 
            Score               : GrabbingTestScores
            Grabs               : GrabbingTestScores
            Attempts            : GrabbingTestScores
        }

    type PointCloudScores = 
        { 
            correctlySelected    : float
            wronglySelected      : float
            correctlyNonSelected : float
            wronglyNonSelected   : float
            selectionOperations  : float
            deselectOperations   : float
            duration             : float
        }

    type ScoresData = 
        { 
            Name                : string

            BasketballScore     : float

            NoFeedback          : GrabbingScoreTypes
            HapticFeedback      : GrabbingScoreTypes
            OpticalFeedback     : GrabbingScoreTypes
            BothFeedback        : GrabbingScoreTypes
            
            PCDesktop           : PointCloudScores
            PCVirtualReality    : PointCloudScores
        }

    let refineData(rawData : string[][]) =
    
        let answersData = Array.sub rawData 1 (rawData.Length - 1)
        let numAnswers = if rawData.Length > 0 then rawData.[0].Length-1 else 0
        
        let data = 
            [ for d in answersData do
                let dataEntry = 
                    {
                        Name             = d.[0]

                        BasketballScore  = Double.Parse(d.[1])

                        NoFeedback       = {
                                                Score = {
                                                            Round0 = Double.Parse(d.[2])
                                                            Round1 = Double.Parse(d.[3])
                                                            Round2 = Double.Parse(d.[4])
                                                            Round3 = Double.Parse(d.[5])
//                                                            Final  = Double.Parse(d.[6])
                                                        }
                                                Grabs = {
                                                            Round0 = Double.Parse(d.[7])
                                                            Round1 = Double.Parse(d.[8])
                                                            Round2 = Double.Parse(d.[9])
                                                            Round3 = Double.Parse(d.[10])
                                                        }
                                                Attempts = {
                                                            Round0 = Double.Parse(d.[11])
                                                            Round1 = Double.Parse(d.[12])
                                                            Round2 = Double.Parse(d.[13])
                                                            Round3 = Double.Parse(d.[14])
                                                        }
                                           }
                        HapticFeedback   = {
                                                Score = {
                                                            Round0 = Double.Parse(d.[15])
                                                            Round1 = Double.Parse(d.[16])
                                                            Round2 = Double.Parse(d.[17])
                                                            Round3 = Double.Parse(d.[18])
//                                                            Final  = Double.Parse(d.[19])
                                                        }
                                                Grabs = {
                                                            Round0 = Double.Parse(d.[20])
                                                            Round1 = Double.Parse(d.[21])
                                                            Round2 = Double.Parse(d.[22])
                                                            Round3 = Double.Parse(d.[23])
                                                        }
                                                Attempts = {
                                                            Round0 = Double.Parse(d.[24])
                                                            Round1 = Double.Parse(d.[25])
                                                            Round2 = Double.Parse(d.[26])
                                                            Round3 = Double.Parse(d.[27])
                                                        }
                                           }
                        OpticalFeedback  = {
                                                Score = {
                                                            Round0 = Double.Parse(d.[28])
                                                            Round1 = Double.Parse(d.[29])
                                                            Round2 = Double.Parse(d.[30])
                                                            Round3 = Double.Parse(d.[31])
//                                                            Final  = Double.Parse(d.[32])
                                                        }
                                                Grabs = {
                                                            Round0 = Double.Parse(d.[33])
                                                            Round1 = Double.Parse(d.[34])
                                                            Round2 = Double.Parse(d.[35])
                                                            Round3 = Double.Parse(d.[36])
                                                        }
                                                Attempts = {
                                                            Round0 = Double.Parse(d.[37])
                                                            Round1 = Double.Parse(d.[38])
                                                            Round2 = Double.Parse(d.[39])
                                                            Round3 = Double.Parse(d.[40])
                                                        }
                                           }
                        BothFeedback     = {
                                                Score = {
                                                            Round0 = Double.Parse(d.[41])
                                                            Round1 = Double.Parse(d.[42])
                                                            Round2 = Double.Parse(d.[43])
                                                            Round3 = Double.Parse(d.[44])
//                                                            Final  = Double.Parse(d.[45])
                                                        }
                                                Grabs = {
                                                            Round0 = Double.Parse(d.[46])
                                                            Round1 = Double.Parse(d.[47])
                                                            Round2 = Double.Parse(d.[48])
                                                            Round3 = Double.Parse(d.[49])
                                                        }
                                                Attempts = {
                                                            Round0 = Double.Parse(d.[50])
                                                            Round1 = Double.Parse(d.[51])
                                                            Round2 = Double.Parse(d.[52])
                                                            Round3 = Double.Parse(d.[53])
                                                        }
                                           }

                        PCDesktop        = { 
                                                correctlySelected    = Double.Parse(d.[54])
                                                wronglySelected      = Double.Parse(d.[55])
                                                correctlyNonSelected = Double.Parse(d.[56])
                                                wronglyNonSelected   = Double.Parse(d.[57])
                                                selectionOperations  = Double.Parse(d.[58])
                                                deselectOperations   = Double.Parse(d.[59])
                                                duration             = DateTime.Parse(d.[60]).Subtract(DateTime.Today).TotalSeconds
                                           }
                        PCVirtualReality = { 
                                                correctlySelected    = Double.Parse(d.[61])
                                                wronglySelected      = Double.Parse(d.[62])
                                                correctlyNonSelected = Double.Parse(d.[63])
                                                wronglyNonSelected   = Double.Parse(d.[64])
                                                selectionOperations  = Double.Parse(d.[65])
                                                deselectOperations   = Double.Parse(d.[66])
                                                duration             = DateTime.Parse(d.[67]).Subtract(DateTime.Today).TotalSeconds
                                           }
                    }
                yield dataEntry
            ] |> List.toArray

        data
