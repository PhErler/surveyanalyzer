﻿namespace SurveyAnalyzer

open QuestionnaireCommon

// calculating: http://vassarstats.net/anova1u.html
// tukey hsd test (msw = MeanSquareVarianceWithinGroups?) https://web.mst.edu/~psyworld/tukeyssteps.htm

module Statistics =
    module Anova =
    
        open Series

        open System.Collections.Generic
        open System.Windows.Forms
        open System.Windows.Forms.DataVisualization.Charting
        
        type Result =
            { 
                Name                : string
                Result              : AnovaResult
                Probability         : float
                GroupMeans          : float[]
                GroupVariances      : float[]
            }
        
        let performAnova(series : Dictionary<string, Series.Series[]>) =
            
            let anova(name : string, series : Series.Series[], probability : float) =
                // Calculate probability
                let probability = 1.0 - probability;
        
                // Make FTest
                let chart = new Chart()
                chart.Series.Clear()

                let seriesNames = [| for i in 0..series.Length-1 do yield (name + "_" + i.ToString()) |]
                let allSeriesString = seriesNames |> String.concat ","
        
                for i in 0..series.Length-1 do
                    let s = series.[i]
                    let newSeries = new Series()
                    newSeries.Name <- seriesNames.[i]
                    chart.Series.Add(newSeries)

                    for i in 0 .. s.Values.Length-1 do
                        let newPoint = new DataPoint(float i, s.Values.[i])
                        newSeries.Points.Add(newPoint)

                let anovaResult = chart.DataManipulator.Statistics.Anova( probability, allSeriesString )
        
                chart.Invalidate()
                chart.Dispose()

                anovaResult


            let probabilities = [| 0.999; 0.99; 0.95; 0.9; 0.8; 0.5; 0.1; 0.01|]

            let results = 
                [|
                    for s in series do
                        let mutable res = AnovaResult()
                        let mutable bestProbability = 0.0
            
                        if s.Value.Length > 1 then
                            for p in probabilities do 
                                if p > bestProbability then
                                    let currRes = anova(s.Key, s.Value, p)
                                    if currRes.FRatio >= currRes.FCriticalValue then
                                        bestProbability <- p
                                        res <- currRes
                                    
                        let means = s.Value |> Array.map(fun se -> se.Mean)
                        let variances = s.Value |> Array.map(fun se -> se.Variance)

                        let result = 
                            {
                                Name = s.Key
                                Probability = bestProbability
                                Result = res
                                GroupMeans = means
                                GroupVariances = variances
                            }
                        yield result
                |]

            results