﻿namespace SurveyAnalyzer

module Session =

    open QuestionnaireCommon
    open PresenceQuestionnaire
    open ScoresQuestionnaire
    open TaskLoadQuestionnaire
    open SicknessQuestionnaire
    
    open System.Collections.Generic
    
    type SessionData = 
        { 
            Name                        : string

            PresenceBasketball          : PresenceQuestionnaireData
            PresenceGrabbingNoFB        : PresenceQuestionnaireData
            PresenceGrabbingHaptic      : PresenceQuestionnaireData
            PresenceGrabbingOptical     : PresenceQuestionnaireData
            PresenceGrabbingBothFB      : PresenceQuestionnaireData
            PresencePointCloudVR        : PresenceQuestionnaireData

            Scores                      : ScoresData

            UsabilityBasketball         : TaskLoadQuestionnaireData
            UsabilityGrabbingNoFB       : TaskLoadQuestionnaireData
            UsabilityGrabbingHaptic     : TaskLoadQuestionnaireData
            UsabilityGrabbingOptical    : TaskLoadQuestionnaireData
            UsabilityGrabbingBothFB     : TaskLoadQuestionnaireData
            UsabilityPointCloudVR       : TaskLoadQuestionnaireData
            UsabilityPointCloudPC       : TaskLoadQuestionnaireData
            
            SicknessBasketball          : SicknessQuestionnaireData
            SicknessGrabbing            : SicknessQuestionnaireData
            SicknessPointCloud          : SicknessQuestionnaireData
        }
        
        
    let makeSessions(presenceData : PresenceQuestionnaireData[], scoresData : ScoresData[], taskLoadData : TaskLoadQuestionnaireData[], sicknessData : SicknessQuestionnaireData[]) =
    
        // presence
        let presenceBasketballDic = new Dictionary<string, PresenceQuestionnaireData>()
        let presenceGrabbingNoFBDic = new Dictionary<string, PresenceQuestionnaireData>()
        let presenceGrabbingHapticDic = new Dictionary<string, PresenceQuestionnaireData>()
        let presenceGrabbingOpticalDic = new Dictionary<string, PresenceQuestionnaireData>()
        let presenceGrabbingBothFBDic = new Dictionary<string, PresenceQuestionnaireData>()
        let presencePointCloudVRDic = new Dictionary<string, PresenceQuestionnaireData>()

        for d in presenceData do
            match d.TestType with
             | "Basketball" ->                                      presenceBasketballDic.Add(d.Name, d)
             | "Grabbing Test - No Feedback" ->                     presenceGrabbingNoFBDic.Add(d.Name, d)
             | "Grabbing Test - Haptic Feedback" ->                 presenceGrabbingHapticDic.Add(d.Name, d)
             | "Grabbing Test - Optical Feedback" ->                presenceGrabbingOpticalDic.Add(d.Name, d)
             | "Grabbing Test - Haptic and Optical Feedback" ->     presenceGrabbingBothFBDic.Add(d.Name, d)
             | "Point-cloud Editing" ->                             presencePointCloudVRDic.Add(d.Name, d)
             | _ -> assert(false)
        
        // scores
        let scoresDic = new Dictionary<string, ScoresData>()
        
        for s in scoresData do
            scoresDic.Add(s.Name, s)
            
        // task load
        let taskLoadBasketballDic = new Dictionary<string, TaskLoadQuestionnaireData>()
        let taskLoadGrabbingNoFBDic = new Dictionary<string, TaskLoadQuestionnaireData>()
        let taskLoadGrabbingHapticDic = new Dictionary<string, TaskLoadQuestionnaireData>()
        let taskLoadGrabbingOpticalDic = new Dictionary<string, TaskLoadQuestionnaireData>()
        let taskLoadGrabbingBothFBDic = new Dictionary<string, TaskLoadQuestionnaireData>()
        let taskLoadPointCloudDesktopDic = new Dictionary<string, TaskLoadQuestionnaireData>()
        let taskLoadPointCloudVRDic = new Dictionary<string, TaskLoadQuestionnaireData>()

        for d in taskLoadData do
            match d.TestType with
             | "Basketball" ->                                      taskLoadBasketballDic.Add(d.Name, d)
             | "Grabbing Test - No Feedback" ->                     taskLoadGrabbingNoFBDic.Add(d.Name, d)
             | "Grabbing Test - Haptic Feedback" ->                 taskLoadGrabbingHapticDic.Add(d.Name, d)
             | "Grabbing Test - Optical Feedback" ->                taskLoadGrabbingOpticalDic.Add(d.Name, d)
             | "Grabbing Test - Haptic and Optical Feedback" ->     taskLoadGrabbingBothFBDic.Add(d.Name, d)
             | "Point-cloud Editing Desktop" ->                     taskLoadPointCloudDesktopDic.Add(d.Name, d)
             | "Point-cloud Editing VR" ->                          taskLoadPointCloudVRDic.Add(d.Name, d)
             | _ -> assert(false)

        // simulator sickness
        let sicknessBasketballDic = new Dictionary<string, SicknessQuestionnaireData>()
        let sicknessGrabbingDic = new Dictionary<string, SicknessQuestionnaireData>()
        let sicknessPointCloudVRDic = new Dictionary<string, SicknessQuestionnaireData>()

        for d in sicknessData do
            match d.TestType with
             | "Basketball" ->                                      sicknessBasketballDic.Add(d.Name, d)
             | "Grabbing Test" ->                                   sicknessGrabbingDic.Add(d.Name, d)
             | "Point-cloud Editing" ->                             sicknessPointCloudVRDic.Add(d.Name, d)
             | _ -> assert(false)

        let sessions = 
            [| for name in presenceBasketballDic.Keys do
                let newSession =    {   
                                        Name =                      name

                                        PresenceBasketball =        presenceBasketballDic.[name]
                                        PresenceGrabbingNoFB =      presenceGrabbingNoFBDic.[name]
                                        PresenceGrabbingHaptic =    presenceGrabbingHapticDic.[name]
                                        PresenceGrabbingOptical =   presenceGrabbingOpticalDic.[name]
                                        PresenceGrabbingBothFB =    presenceGrabbingBothFBDic.[name]
                                        PresencePointCloudVR =      presencePointCloudVRDic.[name]
                                        
                                        Scores =                    scoresDic.[name]
                                        
                                        UsabilityBasketball =       taskLoadBasketballDic.[name]
                                        UsabilityGrabbingNoFB =     taskLoadGrabbingNoFBDic.[name]
                                        UsabilityGrabbingHaptic =   taskLoadGrabbingHapticDic.[name]
                                        UsabilityGrabbingOptical =  taskLoadGrabbingOpticalDic.[name]
                                        UsabilityGrabbingBothFB =   taskLoadGrabbingBothFBDic.[name]
                                        UsabilityPointCloudPC =     taskLoadPointCloudDesktopDic.[name]
                                        UsabilityPointCloudVR =     taskLoadPointCloudVRDic.[name]

                                        SicknessBasketball =        sicknessBasketballDic.[name]
                                        SicknessGrabbing =          sicknessGrabbingDic.[name]
                                        SicknessPointCloud =        sicknessPointCloudVRDic.[name]
                                    }
                yield newSession
            |]

        printfn "Found %A sessions" (sessions.Length)

        sessions
