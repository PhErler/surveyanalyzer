﻿namespace SurveyAnalyzer

module QuestionnaireCommon = 
    open System.IO

    let readData (csvFilePath : string, delimiter : string) = 
        let csvFile = File.ReadAllText(csvFilePath).Replace(System.Environment.NewLine, "\n")
        let lines = csvFile.Split([|'\n'|])                                                 // split at line endings
        let entries = lines |> Array.map(
                                fun d -> d.Replace(delimiter, "@").Split([|'@'|]) 
                                        |> Array.map(fun s -> s.Replace("\"", "").Replace(",", "").Replace("'", ""))) // split at ",", remove "

        let numColumns = if entries.Length > 0 then entries.[0].Length else 0
        printfn "Read %A lines with %A columns from %A" (entries.Length) (numColumns) (csvFilePath)
        entries
