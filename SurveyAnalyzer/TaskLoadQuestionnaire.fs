﻿namespace SurveyAnalyzer

module TaskLoadQuestionnaire =
    open QuestionnaireCommon

    open System
    
    type TaskLoadQuestionnaireData = 
        { 
            Timestamp           : string
            Name                : string
            TestType            : string
            Questions           : string[]
            Answers             : float[]
            TaskLoadScore       : float
            UsabilityScore      : float
            Comment             : string
        }
        
    let refineData(rawData : string[][]) =

        let questions = rawData.[0] |> Array.map (fun q -> q.Replace("\"", "")) 
                                    |> Array.map (fun q -> q.Replace("[", "")) 
                                    |> Array.map (fun q -> q.Replace("]", ""))
                                    |> Array.map (fun q -> q.Replace(" ", "_"))
                                    |> Array.map (fun q -> q.Replace("_", "_"))
                                    |> Array.map (fun q -> q.Replace(":", "_"))

        let numContextQuestions = 2
        let numAnswersTaskLoad = 6
        let numAnswersUsability = 10
        let usabilityStartIndex = (numContextQuestions+numAnswersTaskLoad+1+1)

        let answersData = Array.sub rawData 1 (rawData.Length - 1)
        let questions = Array.concat [|(Array.sub questions (numContextQuestions+1) numAnswersTaskLoad); (Array.sub questions usabilityStartIndex numAnswersUsability)|]

        let maxVal = 4.0

        let refineData(rawData : float[]) =
        
            let invertEntry(entry : float) = 1.0 - entry
        
            // invert all 6 task load questions
            rawData.[0] <- invertEntry(rawData.[0])
            rawData.[1] <- invertEntry(rawData.[1])
            rawData.[2] <- invertEntry(rawData.[2])
            //rawData.[3] <- invertEntry(rawData.[3])
            rawData.[4] <- invertEntry(rawData.[4])
            rawData.[5] <- invertEntry(rawData.[5])
            // rawData.[6] is first comment
            
            // invert 2, 4, 6, 8, 10 of usability
            rawData.[7]  <- invertEntry(rawData.[7])
            rawData.[9]  <- invertEntry(rawData.[9])
            rawData.[11] <- invertEntry(rawData.[11])
            rawData.[13] <- invertEntry(rawData.[13])
            rawData.[15] <- invertEntry(rawData.[15])
        
            rawData
            
        let data = 
            [ for d in answersData do
                
                let answerEntries = Array.concat [|(Array.sub d (numContextQuestions+1) numAnswersTaskLoad); (Array.sub d usabilityStartIndex numAnswersUsability)|]
                let answers = answerEntries |> Array.map(fun e -> Double.Parse(e) / maxVal) |> refineData

                let dataEntry = 
                    {
                        Timestamp           = d.[0]
                        Name                = d.[1] //.Replace(" ", "_")
                        TestType            = d.[2].Replace("\"", "")
                        Questions           = questions
                        Answers             = answers
                        TaskLoadScore       = (answers.[0] + answers.[1] + answers.[2] + answers.[3] + answers.[4] + answers.[5]) / 6.0
                        UsabilityScore      = (answers.[6] + answers.[7] + answers.[8] + answers.[9] + answers.[10] + answers.[11] + answers.[12] + answers.[13] + answers.[14] + answers.[15]) / 10.0
                        Comment             = d.[18]
                    }
                yield dataEntry
            ] |> List.toArray

        data
